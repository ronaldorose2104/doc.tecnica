.. ActividadesP documentation master file, created by
   sphinx-quickstart on Fri Dec 18 19:32:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Actividad 4 de Progroamación III
================================

Python es un sistema de pogramación dinamico, rapido y practico.

Este proyecto fue ejecutado gracias a los tutoriales colocados en la plataforma y
a la información encontrada en YouTube.

Tambien en este proyectose utilizo para crear un modulo sencillo intercmbiable
con una lista de instrucciones que debe dar como resultado el objetivo buscado.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Actividad4_1
   Actividad4_3
