Código fuente
*************

import re

print("Introduzca la placa para buscar")
buscar= input("")

pattern = re.compile(buscar)

with open('Actividad4_2.txt', 'r') as a:
    autos = a.read()

    matches = pattern.finditer(autos)

    for match in matches:
        print(match, " Placa de auto encontrada")

with open('Actividad4_3.txt', 'r') as m:
    motos = m.read()

    matches = pattern.finditer(motos)

    for match in matches:
        print(match, " Placa de moto encontrada")

